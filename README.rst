========================
fao56
========================

.. {# pkglts, doc

.. image:: https://b326.gitlab.io/fao56/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/fao56/1.2.0/

.. image:: https://b326.gitlab.io/fao56/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/fao56

.. image:: https://b326.gitlab.io/fao56/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/fao56/

.. image:: https://badge.fury.io/py/fao56.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/fao56

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/fao56/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/fao56/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/fao56/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/fao56/commits/main
.. #}

Data and formalisms from FAO56 related to crop evapotranspiration

